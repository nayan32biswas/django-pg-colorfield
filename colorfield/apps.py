from django.apps import AppConfig


class DjPgColorFieldConfig(AppConfig):
    name = 'colorfield'
